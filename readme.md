# PineCube Matrix Bot
This is a Matrix bot to send photos output by a pinecube (or any other device) running motion
to an encrypted Matrix room. This allows seeing the useful output of the camera without requiring
third-party infrastructure.

## Implemented
* [x] Catching new files output by motion
* [x] Auto upload to encrypted room
* [x] Auto deletion of uploaded files
* [ ] command to take a photo
* [ ] command to switch to night mode
* [ ] toggle between encrypted and unencrypted rooms

## Cross Compile

Install the following cross compile dependancies (Debian based systems):
```
dpkg --add-architecture armhf
apt install gcc-10-arm-linux-gnueabihf g++-10-arm-linux-gnueabihf libolm-dev:armhf
```

Build with:
```
GOOS=linux GOARCH=arm CGO_ENABLED=1 CC=arm-linux-gnueabihf-gcc-10 PKG_CONFIG_PATH=/usr/lib/arm-linux-gnueabihf go build
```

## Configuration

* server: Matrix Homeserver to connect to
* user: Username on the homeserver
* password: Users password
* monitor: Directory to monitor for changes
* room: Matrix Room to output to
* cryptostore: Disk location to store the crypto state in
* camera: PineCube camera number, currently unused
* deleteOnUpload: Boolean whether to delete uploaded files
* redactAfter: Time to wait before redacting events, suffixes such as s (second), m (minute) and h (hour) work.
