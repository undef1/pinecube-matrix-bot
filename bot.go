package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/fsnotify/fsnotify"
	_ "github.com/mattn/go-sqlite3"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/crypto"
	"maunium.net/go/mautrix/crypto/attachment"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/id"
)

// The following type and three functions allow statestore to implement Mautrix's StateStore
// They are all nops because we don't need this functionality.
type statestore struct{}

func (ss *statestore) IsEncrypted(roomID id.RoomID) bool {
	return true
}

func (ss *statestore) GetEncryptionEvent(roomID id.RoomID) *event.EncryptionEventContent {
	return &event.EncryptionEventContent{
		Algorithm:              id.AlgorithmMegolmV1,
		RotationPeriodMillis:   7 * 24 * 60 * 60 * 1000, // 7 days
		RotationPeriodMessages: 100,
	}
}

func (ss *statestore) FindSharedRooms(userID id.UserID) []id.RoomID {
	return []id.RoomID{}
}

type config struct {
	Server         string
	User           string
	Password       string
	Monitor        string
	Cryptostore    string
	Room           id.RoomID
	Camera         int
	RedactAfter    string
	DeleteOnUpload bool
}

// A basic logger
type logger struct{}

func (l logger) Error(message string, args ...interface{}) {
	fmt.Printf("[ERROR] "+message+"\n", args...)
}
func (l logger) Warn(message string, args ...interface{}) {
	fmt.Printf("[WARN] "+message+"\n", args...)
}
func (l logger) Debug(message string, args ...interface{}) {
	fmt.Printf("[DEBUG] "+message+"\n", args...)
}
func (l logger) Trace(message string, args ...interface{}) {
	fmt.Printf("[TRACE] "+message+"\n", args...)
}

func getUserIDs(client *mautrix.Client, roomID id.RoomID) ([]id.UserID, error) {
	members, err := client.JoinedMembers(roomID)
	if err != nil {
		return []id.UserID{}, err
	}
	userIDs := make([]id.UserID, len(members.Joined))
	i := 0
	for userID := range members.Joined {
		userIDs[i] = userID
		i++
	}
	return userIDs, nil
}

func sendEncrypted(mach *crypto.OlmMachine, client *mautrix.Client, roomID id.RoomID, file io.Reader, size int64, name string) error {
	encfile := attachment.NewEncryptedFile()
	encFileReader := encfile.EncryptStream(file)
	rum := mautrix.ReqUploadMedia{
		Content:       encFileReader,
		ContentLength: size,
		ContentType:   "application/png",
		FileName:      name,
	}
	// This seems to auto-close the file
	uploadResp, err := client.UploadMedia(rum)
	if err != nil {
		fmt.Printf("Could not add media to store: %+v\n", err)
		return err
	}
	encFileInfo := event.EncryptedFileInfo{*encfile, uploadResp.ContentURI.CUString()} // nolint
	mec := event.MessageEventContent{
		MsgType: event.MsgImage,
		Body:    "test",
		File:    &encFileInfo,
	}
	encrypted, err := mach.EncryptMegolmEvent(roomID, event.EventMessage, mec)
	if err == crypto.SessionExpired || err == crypto.SessionNotShared || err == crypto.NoGroupSession {
		IDs, err := getUserIDs(client, roomID)
		if err != nil {
			return err
		}
		err = mach.ShareGroupSession(roomID, IDs)
		if err != nil {
			return err
		}
	}
	if err != nil {
		return err
	}
	resp, err := client.SendMessageEvent(roomID, event.EventEncrypted, encrypted)
	if err != nil {
		return err
	}
	fmt.Println(resp.EventID)
	return nil
}
func sendEncryptedString(mach *crypto.OlmMachine, client *mautrix.Client, roomID id.RoomID, message string) error {
	mec := event.MessageEventContent{
		MsgType: event.MsgImage,
		Body:    message,
	}
	encrypted, err := mach.EncryptMegolmEvent(roomID, event.EventMessage, mec)
	if err == crypto.SessionExpired || err == crypto.SessionNotShared || err == crypto.NoGroupSession {
		IDs, err := getUserIDs(client, roomID)
		if err != nil {
			return err
		}
		err = mach.ShareGroupSession(roomID, IDs)
		if err != nil {
			return err
		}
		encrypted, err = mach.EncryptMegolmEvent(roomID, event.EventMessage, mec)
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	}

	resp, err := client.SendMessageEvent(roomID, event.EventEncrypted, encrypted)
	if err != nil {
		return err
	}
	fmt.Println(resp.EventID)
	return nil
}

func uploadEvents(mach *crypto.OlmMachine, watcher *fsnotify.Watcher, client *mautrix.Client, cfg config) {
	for {
		select {
		case event, ok := <-watcher.Events:
			if !ok {
				return
			}
			fmt.Printf("event: %+v\n", event)
			if event.Op == fsnotify.Create {
				testfile, err := os.Open(event.Name)
				if err != nil {
					fmt.Printf("Could not open file: %+v\n", err)
					continue
				}
				s, err := testfile.Stat()
				if err != nil {
					fmt.Printf("Could not stat file: %+v\n", err)
					continue
				}
				err = sendEncrypted(mach, client, cfg.Room, testfile, s.Size(), event.Name)
				if err != nil {
					fmt.Printf("Could not send encrypted image: %+v\n", err)
					continue
				}
				// rum := mautrix.ReqUploadMedia{
				// 	Content:       testfile,
				// 	ContentLength: s.Size(),
				// 	ContentType:   "application/png",
				// 	FileName:      event.Name,
				// }
				// // This seems to auto-close the file
				// uploadResp, err := client.UploadMedia(rum)
				// if err != nil {
				// 	fmt.Printf("Could not add media to store: %+v\n", err)
				// 	continue
				// }
				// resp, err := client.SendImage(cfg.Room, "New Image", uploadResp.ContentURI)
				// if err != nil {
				// 	fmt.Printf("Could not send image message: %+v\n", err)
				// 	continue
				// }
				// fmt.Println(resp.EventID)
				fmt.Println(cfg.DeleteOnUpload)
				if cfg.DeleteOnUpload {
					fmt.Println("attempting to delete file")
					err = os.Remove(event.Name)
					if err != nil {
						fmt.Printf("Could not delete %s: %+v\n", event.Name, err)
						continue
					}
				}
			}
		case err, ok := <-watcher.Errors:
			if !ok {
				return
			}
			fmt.Printf("Got watcher error: %+v\n", err)
		}
	}
}

func autoredact(ctx context.Context, client *mautrix.Client, cfg config) {
	delOlderThan, err := time.ParseDuration(cfg.RedactAfter)
	if err != nil {
		fmt.Printf("Unable to parse redaction time: %+v\n", err)
		return
	}
	for {
		select {
		case <-ctx.Done():
			return
		default:
			req := mautrix.ReqSync{
				Timeout: 10,
				Context: ctx,
			}
			resp, err := client.FullSyncRequest(req)
			if err != nil {
				fmt.Printf("Error syncing for redactions: %+v\n", err)
				time.Sleep(5 * time.Minute)
				continue
			}
			prevBatch := resp.Rooms.Join[cfg.Room].Timeline.PrevBatch
			messages, err := client.Messages(cfg.Room, prevBatch, "", 'b', 100)
			if err != nil {
				fmt.Printf("Error getting list of messages for redactions: %+v\n", err)
				time.Sleep(5 * time.Minute)
				continue
			}
			for _, evt := range messages.Chunk {
				sent := time.Unix(evt.Timestamp/1000, 0)
				if time.Since(sent) > delOlderThan {
					_, err := client.RedactEvent(cfg.Room, evt.ID)
					if err != nil {
						fmt.Printf("Error redacting event: %+v\n", err)
						continue
					}
				}
			}

		}
		time.Sleep(8 * time.Hour)

	}

}

func main() {
	var cfg config
	cfgFile, err := os.Open("bot.json")
	if err != nil {
		fmt.Printf("Could not open config: %+v\n", err)
		os.Exit(1)
	}
	err = json.NewDecoder(cfgFile).Decode(&cfg)
	if err != nil {
		fmt.Printf("Could not read config: %+v\n", err)
		os.Exit(1)
	}
	err = cfgFile.Close()
	if err != nil {
		fmt.Printf("Could not close cfg file: %+v\n", err)
		os.Exit(1)
	}

	client, err := mautrix.NewClient(cfg.Server, "", "") //, cfg.User, cfg.Token)
	if err != nil {
		fmt.Printf("Could not connect to Matrix server: %+v\n", err)
		os.Exit(1)
	}
	loginresp, err := client.Login(&mautrix.ReqLogin{
		Type: mautrix.AuthTypePassword,
		Identifier: mautrix.UserIdentifier{
			Type: mautrix.IdentifierTypeUser,
			User: cfg.User,
		},
		Password:                 cfg.Password,
		InitialDeviceDisplayName: "PineCube Matrix Bot",
		StoreCredentials:         true,
	})
	if err != nil {
		fmt.Printf("Error logging in: %+v\n", err)
		os.Exit(1)
	}
	fmt.Print(loginresp)
	store := mautrix.NewInMemoryStore()
	client.Store = store
	client.Syncer = mautrix.NewDefaultSyncer()
	client.Client = http.DefaultClient

	db, err := sql.Open("sqlite3", cfg.Cryptostore)
	if err != nil {
		fmt.Printf("Could not open cryptostore: %+v\n", err)
	}
	defer db.Close()
	cryptostore := crypto.NewSQLCryptoStore(db, "sqlite3", "pinecube-matrix-bot", client.DeviceID, []byte("test key"), &logger{})
	// cryptostore, err := crypto.NewGobStore(cfg.Cryptostore)
	if err != nil {
		fmt.Printf("Could not create cryptostore: %+v\n", err)
		os.Exit(1)
	}
	err = cryptostore.CreateTables()
	if err != nil {
		fmt.Printf("Could not create tables: %+v\n", err)
	}

	mach := crypto.NewOlmMachine(client, &logger{}, cryptostore, &statestore{})
	err = mach.Load()
	if err != nil {
		fmt.Printf("Could not load crypto state from store: %+v\n", err)
		os.Exit(1)
	}

	syncer := client.Syncer.(*mautrix.DefaultSyncer)
	syncer.OnSync(mach.ProcessSyncResponse)

	syncer.OnEventType(event.StateMember, func(source mautrix.EventSource, evt *event.Event) {
		mach.HandleMemberEvent(evt)
	})
	start := time.Now().UnixNano() / 1000000
	// Listen to encrypted messages
	syncer.OnEventType(event.EventEncrypted, func(source mautrix.EventSource, evt *event.Event) {
		if evt.Timestamp < start {
			// Ignore events from before the program started
			return
		}
		decrypted, err := mach.DecryptMegolmEvent(evt)
		if err != nil {
			fmt.Println("Failed to decrypt:", err)
			err = sendEncryptedString(mach, client, cfg.Room, fmt.Sprintf("Failed to decrypt: %+v", err))
			if err != nil {
				fmt.Printf("Could not send error back to room: %+v\n", err)
			}
		} else {
			fmt.Println("Received encrypted event:", decrypted.Content.Raw)
			message, isMessage := decrypted.Content.Parsed.(*event.MessageEventContent)
			if isMessage && message.FromDevice != client.DeviceID && message.MsgType == event.MsgText && message.Body != "" {
				err = sendEncryptedString(mach, client, cfg.Room, message.Body)
				if err != nil {
					fmt.Printf("Could not send message back to room: %+v\n", err)
				}
				fmt.Printf("Received Ping: %v\n", message.Body)
			}
		}
	})

	go func() {
		for {
			err = client.Sync()
			if err != nil {
				fmt.Printf("Error Syncing: %+v\n", err)
				time.Sleep(5 * time.Minute)
			}
		}
	}()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go autoredact(ctx, client, cfg)

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		fmt.Printf("Could not start watcher: %+v\n", err)
		os.Exit(1)
	}
	defer watcher.Close()
	err = watcher.Add(cfg.Monitor)
	if err != nil {
		fmt.Printf("Could not add directory to watcher: %+v\n", err)
		os.Exit(1)
	}
	uploadEvents(mach, watcher, client, cfg)

}
