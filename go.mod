module gitlab.com/Undef1/pinecube-matrix-bot

go 1.15

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/mattn/go-sqlite3 v1.14.6
	maunium.net/go/mautrix v0.9.29
)
